package MyImplement;

import java.io.*;
import java.util.Scanner;

public class shufflePhase {
  public static void run() throws IOException {
    //Check how many file Mapping spawn
    File mapFileDirectory = new File("MyImplement/MapFile/");
    int mapFileCount = mapFileDirectory.list().length;

    //Read all file in Mapfile and call shuffle
    for (int id = 0; id < mapFileCount; id++) {
      File pathToSplitFile = new File("MyImplement/MapFile/d" + id + ".txt");
      Scanner inputSplitFile = new Scanner(pathToSplitFile);
      //Call shuffle for each word in file
      while (inputSplitFile.hasNextLine()) {
        String document = inputSplitFile.nextLine();
        shuffling(document, id);
      }

      inputSplitFile.close();
    }
  }

  public static void shuffling(String document, int id) throws IOException {
    //Output file name will be word
		File filePath = new File("MyImplement/ShuffleFile/"
    +document.split(" ")[0]+".txt");
    //If file not exsist new file
		if (!filePath.exists()) {
			PrintWriter newFile = new PrintWriter(filePath);
      newFile.println(document+" d"+id);
      newFile.close();
    //File exsist append
		} else {
      FileWriter appendToFile = new FileWriter(filePath, true);
      PrintWriter exsistingFile = new PrintWriter(appendToFile);
      exsistingFile.println(document+" d"+id);
      exsistingFile.close();
		}
  }
}
