package MyImplement;

import java.io.*;
import java.util.Scanner;

public class tfidf {
  public static void run(String term, String document) throws IOException {
    double result = tf(term, document)*idf(term);
    System.out.println("tf-idf of term " + term + " in document " + document
        + " is: " + result);
  }

  public static double tf(String term, String document) throws IOException {
    double result = 0;
    //Access ShuffleFile  to count "term" in file
    File termShuffleFilePath = new File("MyImplement/ShuffleFile/"+term+".txt");
    Scanner termShuffleFile = new Scanner(termShuffleFilePath);
    double termInDocumentCount=0;

    //Count "term" in file
    while(termShuffleFile.hasNext()){
      String line = termShuffleFile.nextLine();
      String documentContainTerm = line.split(" ")[2];
      if(document.compareTo(documentContainTerm)==0){
        termInDocumentCount++;
      }
    }

    //Count total word in SplitFile
    File splitFile = new File("MyImplement/SplitFile/"+document+".txt");
    Scanner splitFileReader = new Scanner(splitFile);
    double totalWord = (double)(splitFileReader.nextLine().split(" ").length);

    //TF(t,d)
    result = termInDocumentCount/totalWord;

    termShuffleFile.close();
    splitFileReader.close();
    return result;
  }

  public static double idf(String term) throws IOException{
    //Count number of document that have "term" from output of Reduce Phase
    File termReduceFile = new File("MyImplement/ReduceFile/"+term+".txt");
    Scanner termReduceFileReader = new Scanner(termReduceFile);
    String[] termInfo = termReduceFileReader.nextLine().split(" ");
    double numberOfDocumentContainTerm = Double.parseDouble(termInfo[2]);

    //Count total document SplitFile
    File splitFileDirectory = new File("MyImplement/SplitFile/");
    double fileCount = splitFileDirectory.list().length;
    termReduceFileReader.close();

    //If no file have "term"
    if(numberOfDocumentContainTerm<=0){
      return 0;
    }

    // IDF(t)
    double result = Math.log10(fileCount/numberOfDocumentContainTerm);
    return result;
  }
}
