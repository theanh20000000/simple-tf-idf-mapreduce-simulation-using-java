package MyImplement;

import java.io.*;
import java.util.Scanner;

public class splitPhase {
  public static void run() throws IOException {
    String pathToFile = "MyImplement/DataFile/input_tfidf.txt";
    File pathToDataFile = new File(pathToFile);
    Scanner inputFile = new Scanner(pathToDataFile);

    int id = 0;

    while (inputFile.hasNext()) {
      File outputFileToHere = new File("MyImplement/SplitFile/d" + id + ".txt");
      PrintWriter outputFile = new PrintWriter(outputFileToHere);
      String line = inputFile.nextLine();
      outputFile.println(line);
      
      id++;
      outputFile.close();
    }
    inputFile.close();
  }
}
