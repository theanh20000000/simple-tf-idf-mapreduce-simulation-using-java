package MyImplement;

import java.io.*;
import java.util.Scanner;

public class mapPhase {
  public static void run() throws IOException {
    //Read how manny file spawn from Splitting
    File splitFileDirectory = new File("MyImplement/SplitFile/");
    int splitFileCount = splitFileDirectory.list().length;

    //Read all
    for (int id = 0; id < splitFileCount; id++) {
      File pathToSplitFile = new File("MyImplement/SplitFile/d" + id + ".txt");
      Scanner inputSplitFile = new Scanner(pathToSplitFile);

      String document = inputSplitFile.nextLine();
      mapping(document, id);

      inputSplitFile.close();
    }
  }

  public static void mapping(String document, int id) throws IOException {
    //Delete special char
    String[] charBlackList = { ".", ",", "-", "\'", "\"", "(", ")","/" };
    for (int i = 0; i < charBlackList.length; i++) {
      document = document.replace(charBlackList[i], "");
    }

    String[] words = document.split(" ");

    File outputFileToHere = new File("MyImplement/MapFile/d" + id + ".txt");
    PrintWriter outputFile = new PrintWriter(outputFileToHere);

    //Save word and key to it's file
    for (int i = 0; i < words.length; i++) {
      outputFile.println(words[i] + " 1");
    }
    outputFile.close();
  }
}