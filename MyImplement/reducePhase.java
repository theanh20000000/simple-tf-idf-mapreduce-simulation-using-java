package MyImplement;

import java.io.*;
import java.util.Scanner;

public class reducePhase {
  public static void run() throws IOException {
    //Read all file in shuffle file and call reducing
    File pathToShuffleDir = new File("MyImplement/ShuffleFile/");
    File[] listOfFilesInShuffleDir = pathToShuffleDir.listFiles();
    for (int i = 0; i < listOfFilesInShuffleDir.length; i++) {
      reducing(listOfFilesInShuffleDir[i].getName());
    }
  }

  public static void reducing(String fileName) throws IOException {
    //Read file
    File inputFilePath = new File("MyImplement/ShuffleFile/" + fileName);
    Scanner inputFileFromShuffleFile = new Scanner(inputFilePath);

    int documentContainWord = 0;
    String exitsingDocumentCount = "";

    //Read all line
    while (inputFileFromShuffleFile.hasNext()) {
      String line = inputFileFromShuffleFile.nextLine();
      String word = line.split(" ")[0];
      String documentName = line.split(" ")[2];
      File outputFilePath = new File("MyImplement/ReduceFile/"
          + word + ".txt");

      //New and add if file not exist
      if (!outputFilePath.exists()) {
        PrintWriter newFile = new PrintWriter(outputFilePath);
        newFile.println(word + " 1 1");
        newFile.close();
        documentContainWord++;
        exitsingDocumentCount += documentName;
      //Add if file exist
      } else {
        // Đọc file
        Scanner readOutputFile = new Scanner(outputFilePath);
        String buffer = readOutputFile.nextLine();
        String wordKey = buffer.split(" ")[0];
        int wordValue = Integer.parseInt(buffer.split(" ")[1]);
        if (!exitsingDocumentCount.contains(documentName)) {
          documentContainWord++;
          exitsingDocumentCount += documentName;
        }
        //Overwrite file with change value
        PrintWriter newFile = new PrintWriter(outputFilePath);
        newFile.println(wordKey + " "+(wordValue+1)+" "+documentContainWord);
        newFile.close();
        readOutputFile.close();
      }
    }
    inputFileFromShuffleFile.close();
  }
}
